var path = require('path');
var webpack = require('webpack');

module.exports = {
    context: path.resolve(__dirname, './'),
    entry: {
        App: './index.tsx',
        commons: ['jquery']
    },
    output: {
        path: path.resolve(__dirname, "./"),
        filename: "bundles/[name].js",
        publicPath: "http://127.0.0.1:3000"
    },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react']
                    }
                }]
            },
            {
                test: /\.png$/,
                exclude: /node_modules/,
                use: ["url?limit=100000&mimetype=image/png"]
            },
            {
                test: /\.jpg/,
                exclude: /node_modules/,
                use: ['file']
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
                use: ['file?name=public/fonts/[name].[ext]']
            }
        ]
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin("./webpack-commons"),
        //new webpack.optimize.UglifyJsPlugin({minimize: true, compress:{ warnings: false }})
    ]
};
