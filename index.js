const express = require("express");
const exapp = express();
const http = require('http').Server(exapp);
const path = require('path');

// const pg = require('pg');
// const connectionString = 'postgres://Ahmed:hack4islam@localhost:5432/chatapp'
//
// const client = new pg.Client(connectionString);
// client.connect();

const server = exapp.listen(3000, () => {
  console.log('listening on port 3000')
});

const io = require('socket.io').listen(server);

exapp.use( express.static('src/bundles') );

exapp.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});

io.on('connection', function(socket){
  console.log('a user connected');

  //detect when someone joins
  socket.on('userJoined', function(username){
    io.emit("userPresent", username);
  });

  socket.on('sentMessage', function(data){
    io.emit('messageReceived', data);
  });

});
